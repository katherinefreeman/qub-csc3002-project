import 'package:accessi_bel/screens/take_picture_screen.dart';
import 'package:flutter/material.dart';

import 'camera_view_screen.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('AccessiBel'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('Click here to go to Camera View'),
            RaisedButton(
              textColor: Colors.white,
              color: Colors.blue,
              child: Text('Go to Camera View'),
              onPressed: () {
                navigateToCameraViewScreen(context);
              },
            ),
            Text('Click here to Upload Picture'),
            RaisedButton(
              textColor: Colors.white,
              color: Colors.blue,
                child: Text('Go to Upload Picture'),
              onPressed: () {
                navigateToTakePictureScreen(context);
              },
            )
          ],
        ),
      ),
    );
  }
  Future navigateToCameraViewScreen(context) async {
    Navigator.push(context, MaterialPageRoute(builder: (context) => CameraViewScreen()));
  }
  Future navigateToTakePictureScreen(context) async {
    Navigator.push(context, MaterialPageRoute(builder: (context) => TakePictureScreen()));
  }
}
